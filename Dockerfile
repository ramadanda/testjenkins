#our base build image
FROM maven:3.5-jdk-8 AS maven

# copy the project files
COPY ./pom.xml ./pom.xml
COPY ./ojdbc8.jar ./ojdbc8.jar

# build all dependencies
RUN mvn install:install-file -Dfile=ojdbc8.jar -DgroupId=com.oracle -DartifactId=ojdbc8 -Dversion=12.2.0.1 -Dpackaging=jar
RUN mvn dependency:go-offline -B

# copy your other files
COPY ./src ./src

# build for release
RUN mvn clean install -DskipTests

# Use previous built jar for actual docker image
FROM openjdk:8-jdk-alpine

FROM amazonlinux:latest

## Install the dependencies
RUN yum install awscli zip jq util-linux -y

RUN mkdir aes-emp-profile

WORKDIR aes-emp-profile

##Copy the script
COPY deploy/deploy.sh deploy/publish.sh /

COPY --from=maven ./target/aes-emp-profile-1.0-SNAPSHOT-aws.jar /aes-emp-profile




