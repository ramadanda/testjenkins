#
# Description:
# This script will deploy the code to the Lambda fucntion
#
#

BUCKET=$1
LAMBDA_FUNCTION=$2
##Listing the version in the s3 bucket
aws s3api list-object-versions --bucket $BUCKET --query 'Versions[?contains(Key, `ml-aset-reclist.zip`)].{Version: VersionId}' | jq '.' > test.xml

##Taking the lastest version and trimming the value
VERSION=`grep Version test.xml | awk '{print $2}'|  head -1 | cut -c 2- | rev | cut -c 2- | rev`

##Deploying the lastest code to lambda fucntion
echo -e "Starting to deploy the code in $LAMBDA_FUNCTION"
aws lambda update-function-code --function-name $LAMBDA_FUNCTION --s3-bucket $BUCKET --s3-key ml-aset-reclist.zip --s3-object-version $VERSION  --region us-east-1

DEPLOY_STATUS=`echo $?`

if [[ $DEPLOY_STATUS = "0" ]]; then
        echo -e "Deployment Success : Lastet code deployed to $LAMBDA_FUNCTION \n"
else
        echo -e "Deployment Failed : Deployment to Lambda fucntion failed \n"
fi
