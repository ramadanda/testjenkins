#! /bin/sh
#
# Copyright 2019, Blackstraw
#
# Created 2019-02-22 by Sriram Rajaram <Sriram.Rajaram@blackstraw.ai>
#
# Description:
# This script will deploy the code to the Lambda fucntion
#
#


##Push the deploy file to the s3 bucket
LAMBDA=$1
echo -e "started : Pushing the code to s3 bucket \n"
aws s3 cp aes-emp-profile/aes-emp-profile-1.0-SNAPSHOT-aws.jar s3://$LAMBDA
PUBLISH_STATUS=`echo $?`

if [[ $PUBLISH_STATUS = "0" ]]; then
        echo -e "Completed : Pushed the code to s3 bucket \n"
else
        echo -e "Failed : Push failed to s3 bucket \n"
fi



