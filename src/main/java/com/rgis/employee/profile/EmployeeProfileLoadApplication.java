package com.rgis.employee.profile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeProfileLoadApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(EmployeeProfileLoadApplication.class, args);
    }
}
