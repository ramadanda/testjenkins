package com.rgis.employee.profile.model.db;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the AES_EMP_PROFILE_COLUMN_LKUP database table.
 * 
 */
@Entity
@Table(schema = "AES",name="AES_EMP_PROFILE_COLUMN_LKUP")
@NamedQuery(name="AesEmpProfileColumnLkup.findAll", query="SELECT a FROM AesEmpProfileColumnLkup a")
public class AesEmpProfileColumnLkup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="LOOKUP_ID")
	private long lookupId;

	
	@Column(name="RCONNECT_COLUMN_NAME")
	private String rconnectColumnName;

	@Column(name="RCONNECT_TABLE_NAME")
	private String rconnectTableName;
	
	@Column(name="RCONNECT_SCHEMA_NAME")
	private String rconnectSchemaName;
	
	@Column(name="RCONNECT_COLUMN_DATA_TYPE")
	private String rconnectColumnDataType;
	
	@Column(name="AES_SCHEMA_NAME")
	private String aesSchemaName;
	
	@Column(name="AES_COLUMN_DATA_TYPE")
	private String aesColumnDataType;
	

	@Column(name="CREATED_BY")
	private BigDecimal createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="LAST_UPDATE_BY")
	private BigDecimal lastUpdateBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATE_DATE")
	private Date lastUpdateDate;

	@Column(name="AES_COLUMN_NAME")
	private String aesColumnName;

	@Column(name="AES_TABLE_NAME")
	private String aesTableName;

	public AesEmpProfileColumnLkup() {
	}

	public long getLookupId() {
		return this.lookupId;
	}

	public void setLookupId(long lookupId) {
		this.lookupId = lookupId;
	}

	public String getRconnectColumnName() {
		return this.rconnectColumnName;
	}

	public void setRconnectColumnName(String cloudColumnName) {
		this.rconnectColumnName = cloudColumnName;
	}

	public String getRconnectTableName() {
		return this.rconnectTableName;
	}

	public void setRconnectTableName(String cloudTableName) {
		this.rconnectTableName = cloudTableName;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public BigDecimal getLastUpdateBy() {
		return this.lastUpdateBy;
	}

	public void setLastUpdateBy(BigDecimal lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}

	public Date getLastUpdateDate() {
		return this.lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getAesColumnName() {
		return this.aesColumnName;
	}

	public void setAesColumnName(String oracleColumnName) {
		this.aesColumnName = oracleColumnName;
	}

	public String getAesTableName() {
		return this.aesTableName;
	}

	public void setAesTableName(String oracleTableName) {
		this.aesTableName = oracleTableName;
	}
	
	public String getRconnectSchemaName() {
		return rconnectSchemaName;
	}

	public void setRconnectSchemaName(String rconnectSchemaName) {
		this.rconnectSchemaName = rconnectSchemaName;
	}

	public String getRconnectColumnDataType() {
		return rconnectColumnDataType;
	}

	public void setRconnectColumnDataType(String rconnectColumnDataType) {
		this.rconnectColumnDataType = rconnectColumnDataType;
	}

	public String getAesSchemaName() {
		return aesSchemaName;
	}

	public void setAesSchemaName(String aesSchemaName) {
		this.aesSchemaName = aesSchemaName;
	}

	public String getAesColumnDataType() {
		return aesColumnDataType;
	}

	public void setAesColumnDataType(String aesColumnDataType) {
		this.aesColumnDataType = aesColumnDataType;
	}


}