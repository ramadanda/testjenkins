package com.rgis.employee.profile.model.db;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the AES_EMP_PROFILE_TAB_JSON database table.
 * 
 */
@Embeddable
public class AesEmpProfileTabXmlPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "PERSON_ID")
	private long personId;

	@Column(name = "TABLE_NAME")
	private String tableName;

	public AesEmpProfileTabXmlPK() {
	}

	public long getPersonId() {
		return this.personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public String getTableName() {
		return this.tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof AesEmpProfileTabXmlPK)) {
			return false;
		}
		AesEmpProfileTabXmlPK castOther = (AesEmpProfileTabXmlPK) other;
		return (this.personId == castOther.personId) && this.tableName.equals(castOther.tableName);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.personId ^ (this.personId >>> 32)));
		hash = hash * prime + this.tableName.hashCode();

		return hash;
	}
}