package com.rgis.employee.profile.model.db;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the AES_EMP_PROFILE_TAB_JSON database table.
 * 
 */
@Entity
@Table(schema = "AES", name = "AES_EMP_PROFILE_XML_JSON")
@NamedQuery(name = "AesEmpProfileTabXml.findAll", query = "SELECT a FROM AesEmpProfileTabXml a")
public class AesEmpProfileTabXml implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private AesEmpProfileTabXmlPK id;

	@Column(name = "XML_PROPERTIES")
	private String jsonProperties;
	
	@Column(name = "SCHEMA_NAME")
	private String schemaName;
	
	@Column(name = "last_update_date")
	private LocalDate lastUpdateDate;

	@Column(name = "last_update_by")
	private BigDecimal lastUpdatedBy;

	@Column(name = "created_by")
	private BigDecimal createdBy;

	@Column(name = "creation_date")
	private LocalDate creationDate;

	
	public AesEmpProfileTabXml() {
	}

	public AesEmpProfileTabXmlPK getId() {
		return this.id;
	}

	public void setId(AesEmpProfileTabXmlPK id) {
		this.id = id;
	}

	public String getSchemaName() {
		return schemaName;
	}

	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}

	public LocalDate getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(LocalDate lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public BigDecimal getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public BigDecimal getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDate getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDate creationDate) {
		this.creationDate = creationDate;
	}

	public String getJsonProperties() {
		return this.jsonProperties;
	}

	public void setJsonProperties(String jsonProperties) {
		this.jsonProperties = jsonProperties;
	}

}