package com.rgis.employee.profile;

import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rgis.employee.profile.service.MigrationService;

@Component
public class EmployeeProfileLoadFunction implements Function<String, String> {
	@Autowired
	private MigrationService migrationService;

	@Override
	public String apply(String arg0) {
		migrationService.createXmlData();
		migrationService.loadDataToMainTable();
		return "Success";
	}
}
