package com.rgis.employee.profile.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rgis.employee.profile.model.db.AesEmpProfileColumnLkup;
import com.rgis.employee.profile.model.db.AesEmpProfileTabXml;
import com.rgis.employee.profile.repository.oracle.IAesEmpProfileColumnLkupRepository;
import com.rgis.employee.profile.repository.oracle.IAesEmpProfileTabXmlRepository;
import com.rgis.employee.profile.service.MigrationService;
import com.rgis.employee.profile.util.CommonConstants;

@Component("migrationService")
public class MigrationServiceImpl implements MigrationService {

	private static final Logger logger = LoggerFactory.getLogger(MigrationServiceImpl.class);

	@Autowired
	private IAesEmpProfileColumnLkupRepository columnLookUpRepository;
	@Autowired
	private IAesEmpProfileTabXmlRepository jsonRepository;
	@Autowired
	@PersistenceContext(unitName = "oracle")
	private EntityManager emOracle;

	@PersistenceContext(unitName = "cloud")
	private EntityManager emCloud;

	@Override
	public void createXmlData() {
		try {

			jsonRepository.deleteAll();
			Map<String, Map<String, List<AesEmpProfileColumnLkup>>> groupBySchemaAndTableName = columnLookUpRepository
					.findAll().stream().collect(Collectors.groupingBy(AesEmpProfileColumnLkup::getRconnectSchemaName,
							Collectors.groupingBy(AesEmpProfileColumnLkup::getRconnectTableName)));

			groupBySchemaAndTableName.entrySet().forEach(schemaEntry -> {

				Map<String, List<AesEmpProfileColumnLkup>> groupByTableName = schemaEntry.getValue();
				groupByTableName.entrySet().forEach(entry -> {
					StringBuilder[] queryStr = new StringBuilder[1];
					queryStr[0] = new StringBuilder();
					List<AesEmpProfileColumnLkup> rowList = entry.getValue();
					rowList.forEach(row -> {

						queryStr[0] = (queryStr[0])
								.append(queryStr[0] != null && queryStr[0].length() > 0 ? CommonConstants.COMMA
										: CommonConstants.SPACE)
								.append(row.getRconnectColumnName());
					});

					Query query = emCloud.createNativeQuery(CommonConstants.SELECT + CommonConstants.SPACE
							+ CommonConstants.PERSONID + CommonConstants.SPACE + CommonConstants.AS
							+ CommonConstants.SPACE + CommonConstants.PERSONID + CommonConstants.COMMA
							+ CommonConstants.APSTROPHE + entry.getKey() + CommonConstants.APSTROPHE
							+ CommonConstants.SPACE + CommonConstants.AS + CommonConstants.SPACE
							+ CommonConstants.TABLE_NAME + CommonConstants.COMMA + CommonConstants.XML_FOREST
							+ CommonConstants.OPEN_BRACES + queryStr[0] + CommonConstants.CLOSE_BRACES
							+ CommonConstants.SPACE + CommonConstants.AS + CommonConstants.SPACE
							+ CommonConstants.PROPERTIES + CommonConstants.COMMA + CommonConstants.AUDIT_FIELDS
							+ CommonConstants.COMMA + CommonConstants.APSTROPHE + schemaEntry.getKey()
							+ CommonConstants.APSTROPHE + CommonConstants.SPACE + CommonConstants.AS
							+ CommonConstants.SPACE + CommonConstants.SCHEMA_NAME + CommonConstants.SPACE
							+ CommonConstants.FROM + CommonConstants.SPACE + schemaEntry.getKey()
							+ CommonConstants.DOT_OPERATOR + entry.getKey(), AesEmpProfileTabXml.class);
					List<AesEmpProfileTabXml> jsonRecordList = query.getResultList();
					jsonRepository.saveAll(jsonRecordList);

				});

			});

		} catch (Exception ex) {
			logger.error("Failed to create createXmlData ", ex.getMessage());

		}

	}

	@Transactional
	@Override
	public void loadDataToMainTable() {
		try {

			columnLookUpRepository.findAll().forEach(entity -> {

				Query query = emCloud.createNativeQuery(CommonConstants.SELECT + CommonConstants.SPACE
						+ CommonConstants.PERSONID + CommonConstants.SPACE + CommonConstants.AS + CommonConstants.SPACE
						+ CommonConstants.PERSONID + CommonConstants.COMMA + entity.getRconnectColumnName()

						+ CommonConstants.SPACE + CommonConstants.AS + CommonConstants.SPACE
						+ CommonConstants.COLUMN_VAUE + CommonConstants.SPACE + CommonConstants.FROM
						+ CommonConstants.SPACE + entity.getRconnectSchemaName() + CommonConstants.DOT_OPERATOR
						+ entity.getRconnectTableName());

				List<Object[]> recordList = query.getResultList();

				recordList.forEach(record -> {
					try {
						Query oracleQueryCount = emOracle.createNativeQuery(CommonConstants.COUNT_SQL
								+ CommonConstants.SPACE + CommonConstants.FROM + CommonConstants.SPACE
								+ entity.getAesSchemaName() + CommonConstants.DOT_OPERATOR + entity.getAesTableName()

								+ CommonConstants.SPACE + CommonConstants.WHERE + CommonConstants.SPACE
								+ CommonConstants.PERSONID + CommonConstants.EQUALS + record[0]);
						BigDecimal count = (BigDecimal) oracleQueryCount.getSingleResult();

						if (count.intValue() > 0) {
							String equalStr = entity.getRconnectColumnDataType().equalsIgnoreCase(
									CommonConstants.VARCHAR) ? CommonConstants.EQUALS + CommonConstants.APSTROPHE
											: CommonConstants.EQUALS;
							String endString = entity.getRconnectColumnDataType().equalsIgnoreCase(
									CommonConstants.VARCHAR) ? CommonConstants.APSTROPHE : CommonConstants.SPACE;

							Query oracleQuery = emOracle.createNativeQuery(
									CommonConstants.UPDATE + CommonConstants.SPACE + entity.getAesSchemaName()
											+ CommonConstants.DOT_OPERATOR + entity.getAesTableName()
											+ CommonConstants.SPACE + CommonConstants.SET + CommonConstants.SPACE
											+ entity.getAesColumnName() + equalStr + record[1] + endString

											+ CommonConstants.SPACE + CommonConstants.WHERE + CommonConstants.SPACE
											+ CommonConstants.PERSONID + CommonConstants.EQUALS + record[0]);
							oracleQuery.executeUpdate();
						} else {

							Query oracleQuery = emOracle.createNativeQuery(CommonConstants.INSERT_INTO
									+ CommonConstants.SPACE + entity.getAesSchemaName() + CommonConstants.DOT_OPERATOR
									+ entity.getAesTableName() + CommonConstants.OPEN_BRACES + CommonConstants.PERSONID
									+ CommonConstants.COMMA + entity.getAesColumnName() + CommonConstants.OPEN_BRACES
									+ CommonConstants.VALUES + CommonConstants.CLOSE_BRACES + record[0]
									+ CommonConstants.COMMA + record[1]

									+ CommonConstants.CLOSE_BRACES);
							oracleQuery.executeUpdate();

						}

					} catch (Exception ex) {
						logger.error("Failed to loadDataToMainTable ", ex.getMessage());

					}
				});

			});

		} catch (Exception ex) {
			logger.error("Failed to loadDataToMainTable", ex.getMessage());
		}
	}
}