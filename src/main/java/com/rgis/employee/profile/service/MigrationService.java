package com.rgis.employee.profile.service;

import org.springframework.stereotype.Service;

@Service
public interface MigrationService {

	void createXmlData();

	void loadDataToMainTable();

}