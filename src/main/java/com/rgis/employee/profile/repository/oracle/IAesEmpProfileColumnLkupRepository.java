package com.rgis.employee.profile.repository.oracle;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rgis.employee.profile.model.db.AesEmpProfileColumnLkup;

public interface IAesEmpProfileColumnLkupRepository extends JpaRepository<AesEmpProfileColumnLkup, BigDecimal> {

}
