package com.rgis.employee.profile.repository.oracle;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rgis.employee.profile.model.db.AesEmpProfileTabXml;

public interface IAesEmpProfileTabXmlRepository extends JpaRepository<AesEmpProfileTabXml, BigDecimal> {
	
	Optional<List<AesEmpProfileTabXml>> findByIdTableName(String tableName);

}
