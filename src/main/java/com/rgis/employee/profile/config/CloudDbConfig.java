package com.rgis.employee.profile.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.persistenceunit.PersistenceUnitManager;
import org.springframework.orm.jpa.vendor.AbstractJpaVendorAdapter;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "cloudEntityManagerFactory", transactionManagerRef = "cloudTransactionManager", basePackages = {
		"com.rgis.employee.profile.model.db", "com.rgis.employee.profile.repository.cloud" })
public class CloudDbConfig {

	@Autowired(required = false)
	private PersistenceUnitManager persistenceUnitManager;

	@Bean(name = "cloudJpaProperties")
	@ConfigurationProperties("spring.cloud.datasource.jpa")
	public JpaProperties cloudJpaProperties() {
		return new JpaProperties();
	}

	@Bean(name = "cloudDataSource")
	@ConfigurationProperties(prefix = "spring.cloud.datasource")
	public DataSource dataSource() {

		return DataSourceBuilder.create().build();
	}

	@Bean(name = "cloudEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(
			@Qualifier("cloudJpaProperties") JpaProperties cloudJpaProperties,
			@Qualifier("cloudDataSource") DataSource dataSource) {
		EntityManagerFactoryBuilder builder = createEntityManagerFactoryBuilder(cloudJpaProperties);
		return builder.dataSource(dataSource)
				.packages("com.rgis.employee.profile.model.db", "com.rgis.employee.profile.repository.cloud")
				.persistenceUnit("cloud").properties(cloudJpaProperties.getProperties())
				.build();
	}

	private EntityManagerFactoryBuilder createEntityManagerFactoryBuilder(JpaProperties cloudJpaProperties) {
		JpaVendorAdapter jpaVendorAdapter = createJpaVendorAdapter(cloudJpaProperties);
		return new EntityManagerFactoryBuilder(jpaVendorAdapter, cloudJpaProperties.getProperties(),
				this.persistenceUnitManager);
	}

	private JpaVendorAdapter createJpaVendorAdapter(JpaProperties jpaProperties) {
		return getJpaVendorAdapter(jpaProperties);
	}

	static JpaVendorAdapter getJpaVendorAdapter(JpaProperties jpaProperties) {
		AbstractJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
		adapter.setShowSql(jpaProperties.isShowSql());
		adapter.setDatabase(jpaProperties.getDatabase());
		adapter.setDatabasePlatform(jpaProperties.getDatabasePlatform());
		adapter.setGenerateDdl(jpaProperties.isGenerateDdl());
		return adapter;
	}

	@Bean(name = "cloudTransactionManager")
	public PlatformTransactionManager transactionManager(
			@Qualifier("cloudEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}
}
