package com.rgis.employee.profile.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.persistenceunit.PersistenceUnitManager;
import org.springframework.orm.jpa.vendor.AbstractJpaVendorAdapter;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "oracleEntityManagerFactory", transactionManagerRef = "oracleTransactionManager", basePackages = {
		"com.rgis.employee.profile.model.db", "com.rgis.employee.profile.repository.oracle" })
public class OracleDbConfig {

	@Autowired(required = false)
	private PersistenceUnitManager persistenceUnitManager;

	@Primary
	@Bean(name = "oracleJpaProperties")
	@ConfigurationProperties("spring.oracle.datasource.jpa")
	public JpaProperties oracleJpaProperties() {
		return new JpaProperties();
	}

	@Primary
	@Bean(name = "oracleDataSource")
	@ConfigurationProperties(prefix = "spring.oracle.datasource")
	public DataSource dataSource() {
		return DataSourceBuilder.create().build();
	}

	@Primary
	@Bean(name = "oracleEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(
			@Qualifier("oracleJpaProperties") JpaProperties oracleJpaProperties,
			@Qualifier("oracleDataSource") DataSource dataSource) {
		EntityManagerFactoryBuilder builder = createEntityManagerFactoryBuilder(oracleJpaProperties);
		return builder.dataSource(dataSource)
				.packages("com.rgis.employee.profile.model.db", "com.rgis.employee.profile.repository.oracle")
				.persistenceUnit("oracle")
				.properties(oracleJpaProperties.getProperties()).build();
	}

	private EntityManagerFactoryBuilder createEntityManagerFactoryBuilder(JpaProperties oracleJpaProperties) {
		JpaVendorAdapter jpaVendorAdapter = createJpaVendorAdapter(oracleJpaProperties);
		return new EntityManagerFactoryBuilder(jpaVendorAdapter, oracleJpaProperties.getProperties(),
				this.persistenceUnitManager);
	}

	private JpaVendorAdapter createJpaVendorAdapter(JpaProperties jpaProperties) {
		return getJpaVendorAdapter(jpaProperties);
	}

	static JpaVendorAdapter getJpaVendorAdapter(JpaProperties jpaProperties) {
		AbstractJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
		adapter.setShowSql(jpaProperties.isShowSql());
		adapter.setDatabase(jpaProperties.getDatabase());
		adapter.setDatabasePlatform(jpaProperties.getDatabasePlatform());
		adapter.setGenerateDdl(jpaProperties.isGenerateDdl());
		return adapter;
	}

	@Primary
	@Bean(name = "oracleTransactionManager")
	public PlatformTransactionManager transactionManager(
			@Qualifier("oracleEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}
}
