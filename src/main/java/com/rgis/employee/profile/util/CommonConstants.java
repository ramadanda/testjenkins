package com.rgis.employee.profile.util;

public class CommonConstants {

	public final static String SELECT = "SELECT";
	public final static String PERSONID = "person_id";
	public final static String INSERT_INTO = "INSERT INTO";
	public final static String UPDATE = "UPDATE";
	public final static String WHERE = "WHERE";
	public final static String SET = "SET";
	public final static String COMMA = ",";
	public final static String APSTROPHE = "'";
	public final static String EQUALS = " = ";
	public final static String AS = "AS";
	public final static String TABLE_NAME = "table_name";
	public final static String SPACE = " ";
	public final static String XML_FOREST = "xmlforest";
	public final static String OPEN_BRACES = "(";
	public final static String CLOSE_BRACES = ")";
	public final static String AUDIT_FIELDS = "22214 AS created_by,22214 AS last_update_by,current_timestamp AS creation_date,current_timestamp AS LAST_UPDATE_DATE";
	public final static String SCHEMA_NAME = "schema_name";
	public final static String FROM = "FROM";
	public final static String DOT_OPERATOR = ".";
	public final static String PROPERTIES = "JSON_PROPERTIES";
	public final static String COLUMN_VAUE = "columnValue";
	public final static String COUNT_SQL = "SELECT count(*)";
	public final static String VALUES = "VALUES";
	public final static String VARCHAR = "VARCHAR";

}
